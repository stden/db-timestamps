package tests;

import org.junit.Before;
import org.junit.Test;
import timestamps.DataSource;
import timestamps.Event;
import timestamps.EventsDatabase;
import timestamps.MySQLDataSource;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class EventsDatabaseTest {
    @Before
    public void setUp() throws IOException, SQLException {
        EventsDatabase db = new EventsDatabase(new MySQLDataSource());
        db.initDatabase("DB.SQL");
        db.clearEvents();
    }

    @Test
    public void testInsertOneEvent() {
        EventsDatabase db = new EventsDatabase(new MySQLDataSource());

        LocalDateTime now = LocalDateTime.now().withNano(0);
        db.addEvent(now);

        assertEquals(0, db.getEventsList().size());

        db.saveEventQueue();

        List<Event> events = db.getEventsList();
        assertEquals(1, events.size());
        assertEquals(now, events.get(0).datetime);
    }

    @Test
    public void testInsertManyEvents() throws InterruptedException {
        EventsDatabase db = new EventsDatabase(new MySQLDataSource());

        final int EVENTS = 5;

        for (int i = 0; i < EVENTS; i++) {
            LocalDateTime now = LocalDateTime.now().withNano(0);
            db.addEvent(now);
            Thread.sleep(500);
        }
        assertEquals(0, db.getEventsList().size());

        db.saveEventQueue();

        List<Event> events = db.getEventsList();
        assertEquals(EVENTS, events.size());

        db.showEventsList();
    }

    @Test
    public void testSimulateLostConnection() throws SQLException, InterruptedException {
        new EventsDatabase(new MySQLDataSource()).clearEvents();

        EventsDatabase db = new EventsDatabase(new DataSource() {
            int count = 0;

            DataSource mySQLDataSource = new MySQLDataSource();

            @Override
            public Connection getConnection() throws SQLException {
                count++;
                if (count == 2) {
                    throw new SQLException("Can't connect to database");
                }
                return mySQLDataSource.getConnection();
            }
        });

        final int EVENTS = 5;

        for (int i = 0; i < EVENTS; i++) {
            LocalDateTime now = LocalDateTime.now().withNano(0);
            db.addEvent(now);
            Thread.sleep(500);
        }
        assertEquals(0, db.getEventsList().size());

        db.saveEventQueue();

        List<Event> events = db.getEventsList();
        assertEquals(EVENTS, events.size());

        db.showEventsList();
    }
}
