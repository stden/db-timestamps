package timestamps;

import java.time.LocalDateTime;

public class Event {
    public final int id;
    public final LocalDateTime datetime;

    public Event(int id, LocalDateTime datetime) {
        this.id = id;
        this.datetime = datetime;
    }
}
