package timestamps;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.SocketException;
import java.sql.SQLException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import static java.util.concurrent.TimeUnit.SECONDS;

public class Main {
    private static final Logger log = LogManager.getLogger(Main.class);

    public static void main(String[] args) throws InterruptedException, IOException, SQLException {
        EventsDatabase db = new EventsDatabase(new MySQLDataSource());
        // Creating initial database schema
        db.initDatabase("DB.SQL"); // TODO: Закомментировать после первого запуска
        switch (args.length) {
            case 0:
                // TODO: предотвращение повторного запуска. Если не нужно => закомментируйте следующую строку
                preventLaunchingMultipleInstances();

                log.info("When started without parameters, application once a second writes current time (timestamp) to database");
                log.info("To interrupt the process, please, press 'Enter'...");

                final ScheduledExecutorService scheduler =
                        Executors.newScheduledThreadPool(1);
                final ScheduledFuture<?> future =
                        scheduler.scheduleAtFixedRate(db::writeCurrentTime, 0, 1, SECONDS);

                while (System.in.available() == 0) {
                    Thread.sleep(1000); // Пауза 1 секунда
                }
                future.cancel(true);
                scheduler.shutdown();
                break;
            case 1:
                // Starting with the "-p" option application shows events list and exit
                if (args[0].equalsIgnoreCase("-p")) {
                    db.showEventsList();
                }
                // Creating initial database structure by command line key "-c" (Create)
                if (args[0].equalsIgnoreCase("-c")) {
                    db.initDatabase("DB.SQL");
                }
                break;
        }
    }

    /**
     * Предотвращение повторного запуска приложения
     * Если одновременно две копии приложения потеряют соединение с базой данных
     * и потом будут асинхронно записывать события из очереди =>
     * порядок может нарушаться => нужно предотвратить повторный запуск приложения
     * Для предотвращения повторного запуска при старте открываем порт,
     * если порт не открывается => говорим что приложение уже запущено и завершаем работу
     */
    private static void preventLaunchingMultipleInstances() {
        try {
            int port = 12345;
            try (ServerSocket socket = new ServerSocket()) {
                socket.bind(new InetSocketAddress(port));
                log.info("Port " + port + " opened");
            }
        } catch (SocketException e) {
            log.error("Application already running - stop it first!");
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}