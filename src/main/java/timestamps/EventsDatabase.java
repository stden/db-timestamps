package timestamps;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.FileUtils;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class EventsDatabase {
    private static final Logger log = LogManager.getLogger(EventsDatabase.class);

    private static final DateTimeFormatter FORMAT = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");

    private LinkedBlockingQueue<LocalDateTime> eventQueue = new LinkedBlockingQueue<>();

    private DataSource dataSource;

    public EventsDatabase(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void writeCurrentTime() {
        addEvent(LocalDateTime.now());
        new Thread(this::saveEventQueue).start();
    }

    public void addEvent(LocalDateTime dt) {
        log.info("Add to queue: " + dt.format(FORMAT));
        eventQueue.add(dt);
    }

    public void saveEventQueue() {
        while (!eventQueue.isEmpty()) {
            if (trySaveEventQueue()) {
                return;
            }
            log.info("Retry connection in 5 seconds...");
            try {
                Thread.sleep(5 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private synchronized boolean trySaveEventQueue() {
        try (Connection con = dataSource.getConnection()) {
            while (!eventQueue.isEmpty()) {
                LocalDateTime dt = eventQueue.peek();
                try (PreparedStatement stmt = con.prepareStatement("INSERT INTO event(datetime) VALUES(?)")) {
                    stmt.setTimestamp(1, Timestamp.valueOf(dt));
                    int rows = stmt.executeUpdate();
                    if (rows != 1) {
                        log.error("ERROR: Can't insert event");
                        return false;
                    }
                }
                eventQueue.take();
            }
            log.info("All data was saved to database.");
            return true; // Successfully saved all eventQueue
        } catch (SQLException | InterruptedException ex) {
            log.error(ex.getMessage());
            return false;
        }
    }

    public List<Event> getEventsList() {
        List<Event> events = new LinkedList<>();
        try (Connection con = dataSource.getConnection()) {
            try (Statement stmt = con.createStatement()) {
                Timestamp prev = null;
                try (ResultSet rs = stmt.executeQuery("SELECT id, datetime FROM event")) {
                    while (rs.next()) {
                        int id = rs.getInt("id");
                        Timestamp timestamp = rs.getTimestamp("datetime");
                        events.add(new Event(id, timestamp.toLocalDateTime()));
                        // При выводе данных на экран timestamp должен быть в Ascending Order без применения
                        // сортировки
                        if (prev != null && timestamp.before(prev)) {
                            throw new RuntimeException("ERROR: нарушен порядок событий");
                        }
                        prev = timestamp;
                    }
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return events;
    }

    public void showEventsList() {
        log.info("== Database content ==");
        getEventsList().stream()
                .map(event -> event.id + ". " + event.datetime.format(FORMAT))
                .forEach(log::info);
    }

    public void initDatabase(String sqlFileName) throws SQLException, IOException {
        log.info("== Database initialization ==");
        try (Connection con = dataSource.getConnection()) {
            String sqlScript = FileUtils.fileToStr(sqlFileName);
            for (String sql : sqlScript.split(";")) {
                if (!sql.trim().isEmpty()) {
                    try (Statement stmt = con.createStatement()) {
                        stmt.execute(sql);
                    }
                }
            }
        }
    }

    public void clearEvents() throws SQLException {
        try (Connection con = dataSource.getConnection()) {
            try (Statement stmt = con.createStatement()) {
                stmt.execute("DELETE FROM event");
            }
        }
    }
}
