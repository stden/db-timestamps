package timestamps;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQLDataSource implements DataSource {
    private static final String DB_NAME = "timestamps";
    // TODO: для локального запуска заменить mysql на localhost
    private static final String HOST = "mysql";
    private static final String URL = "jdbc:mysql://" + HOST + ":3306/" + DB_NAME;
    private static final String MYSQL_USER = "root";
    private static final String MYSQL_PASSWORD = "mysql_strong_password";

    /**
     * @return Database connection to MySQL
     * @throws SQLException Database access error or other errors
     */
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(URL, MYSQL_USER, MYSQL_PASSWORD);
    }
}
